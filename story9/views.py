from django.shortcuts import render, redirect
from .forms import formLogin, formRegister
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

# Create your views here.
def login_view(request):
    form = formLogin()
    flag = False
    if request.method == 'POST':
        form = formLogin(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                flag = True
                response = {'form':form,'user':user,'flag':flag}
                return redirect('/login')
            else:
                response = {'form':form,'user':user,'flag':flag}
                return redirect('/login')

    else:
        response = {'form':form,'flag':flag}
        return render(request,'halLogin.html',response)

def register_view(request):
    if request.user.is_authenticated:
        return redirect('/register')
    else:
        form = formRegister()
        if request.method == 'POST':
            form = formRegister(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request,'Akun berhasil dibuat!' + user)
                return redirect('/login')

        context = {'form':form}
        return render(request,'halRegister.html',context)

def logout_view(request):
    logout(request)
    form = formLogin()
    response = {'form':form}
    return redirect('/login')
