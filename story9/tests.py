from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from .forms import formLogin, formRegister

# Create your tests here.
class unitTest(TestCase):
    def test_hal_login(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code,200)
    
    def test_hal_register(self):
        response = Client().get('/register')
        self.assertEqual(response.status_code,200)

    def test_hal_logout(self):
        response = Client().get('/logout')
        self.assertEqual(response.status_code,302)
    
    def test_template_register(self):
        response = Client().get('/register')
        self.assertEqual(response.status_code,200)

    def test_template_login(self):
        response = Client().get('/login')
        self.assertTemplateUsed(response,'halLogin.html')
    
    def test_form_login_valid(self):
        form_login = formLogin(data={'username':"aaaa" , 'password':"abcdabcd"})
        self.assertTrue(form_login.is_valid())
        self.assertEqual(form_login.cleaned_data['username'],"aaaa")
        self.assertEqual(form_login.cleaned_data['password'],"abcdabcd")

    def test_form_register_valid(self):
        form_register = formRegister(data={
            'username':"aaaa" ,
            'email' : 'bbbbb@gmail.com', 
            'password1':"abcdabcd",
            'password2':"abcdabcd"
        })
        self.assertTrue(form_register.is_valid())
        self.assertEqual(form_register.cleaned_data['username'],"aaaa")
        self.assertEqual(form_register.cleaned_data['email'],"bbbbb@gmail.com")
        self.assertEqual(form_register.cleaned_data['password1'],"abcdabcd")
        self.assertEqual(form_register.cleaned_data['password2'],"abcdabcd")
        response = Client().get('/logout')
        self.assertRedirects(response,'/login')

    def test_logout_valid(self):
        self.client.logout
        response = Client().get('/logout')
        self.assertRedirects(response,'/login')


