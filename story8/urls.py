from django.urls import path
from . import views

app_name = 'story8'

urlpatterns = [
    path('story8/', views.index, name='index'),
    path('story8/data',views.data, name='data_buku')
]