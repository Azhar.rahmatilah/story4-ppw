from django.test import TestCase, Client
from django.urls import reverse

# Create your tests here.
class UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code,200)

    def test_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response,'halstory8.html')

    def test_json_url(self):
        response = Client().get('/story8/data')
        self.assertEqual(response.status_code,200)

    def test_html_content(self):
        response = self.client.get(reverse('story8:index'))
        data = response.content.decode('utf8')
        self.assertIn("No", data)
        self.assertIn("Cover", data)
        self.assertIn("Title", data)
        self.assertIn("Author", data)
