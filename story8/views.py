from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

# Create your views here.
def index(request):
    response = {}
    return render(request,'halstory8.html',response)

def data(request):
    try:
        tes = request.GET['q']
    except:
        tes = "a"
    
    json_read = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + tes).json()

    return JsonResponse(json_read)
