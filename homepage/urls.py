from django.urls import path
from . import views
#from .views import Join, sched_delete

app_name = 'homepage'

urlpatterns = [
    path('', views.Story3, name='Story3'),
    path('kenaldekat/', views.kenaldekat, name='kenaldekat'),
    path('skill/', views.halskill, name='halskill'),
    path('rpendidikan/', views.halrp, name='halrp'),
    path('hobi/', views.halhobi, name='halhobi'),
    path('pengalaman/', views.halpengalaman, name='halpengalaman'),
    path('kontak/', views.halkontak, name='halkontak'),
    path('detailjadwal/', views.DetailJadwal, name='DetailJadwal'),
    path('detailjadwal/<int:pk>', views.sched_delete, name = 'delete'),
    path('Jadwal/', views.Join , name = 'Jadwal')
    # dilanjutkan ...
]