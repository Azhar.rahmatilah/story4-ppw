from django.db import models

# Create your models here.
class Jadwal(models.Model):
    Matkul = models.TextField(max_length = 50)
    Dosen = models.TextField(max_length = 50)
    SKS = models.TextField(max_length = 50)
    Deskripsi = models.TextField(max_length = 50)
    Semester = models.TextField(max_length = 50)
    Ruang = models.TextField(max_length = 50)