from django.shortcuts import render, redirect
from .models import Jadwal as jadwal
from .forms import JadwalForm

# Create your views here.
def Story3(request):
    return render(request,'Story3.html')

def kenaldekat(request):
    return render(request,'kenaldekat.html')

def halskill(request):
    return render(request,'halskill.html')

def halrp(request):
    return render(request,'halrp.html')

def halhobi(request):
    return render(request,'halhobi.html')

def halpengalaman(request):
    return render(request,'halpengalaman.html')

def halkontak(request):
    return render(request,'halkontak.html')

def DetailJadwal(request):
    data = jadwal.objects.all()
    return render(request,'DetailJadwal.html' , {"sched" : data})

def Join(request):
    if request.method == "POST":
        form = JadwalForm(request.POST)
        if form.is_valid():
            sched = jadwal()
            sched.Matkul = form.cleaned_data['Matkul']
            sched.Dosen = form.cleaned_data['Dosen']
            sched.SKS = form.cleaned_data['SKS']
            sched.Deskripsi = form.cleaned_data['Deskripsi']
            sched.Semester = form.cleaned_data['Semester']
            sched.Ruang = form.cleaned_data['Ruang']
            sched.save()
            return redirect('/detailjadwal')
    else :
        sched = jadwal.objects.all()
        form = JadwalForm()
        response = {"sched":sched, 'form': form}
        return render(request,'Schedule.html',response)

def sched_delete(request, pk):
    if request.method =="POST":
        form = JadwalForm(request.POST)
        if form.is_valid():
            sched = jadwal()
            sched.Matkul = form.cleaned_data['Matkul']
            sched.Dosen = form.cleaned_data['Dosen']
            sched.SKS = form.cleaned_data['SKS']
            sched.Deskripsi = form.cleaned_data['Deskripsi']
            sched.Semester = form.cleaned_data['Semester']
            sched.Ruang = form.cleaned_data['Ruang']
            sched.save()
            return redirect('/Jadwal')
    else :
        jadwal.objects.filter(pk=pk).delete()
        data = jadwal.objects.all()
        form = JadwalForm()
        response = {"sched" : data, 'form' : form}
        return render(request,'DetailJadwal.html',response)
