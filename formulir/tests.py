from django.test import TestCase
from django.urls import reverse
from .models import Form1,Form2,Form3
from .forms import Form_Futsal,Form_Traveling,Form_Jogging

class UnitTest(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get(reverse('formulir:kegiatan'))
        self.assertEqual(response.status_code, 200)

    def test_template_yang_digunakan_dari_halaman_kegiatan(self):
        response = self.client.get(reverse('formulir:kegiatan'))
        self.assertTemplateUsed(response, 'kegiatan.html')

    def test_models_dari_halaman_kegiatan(self):
        Form1.objects.create(Nama='abc')
        n_futsal = Form1.objects.all().count()
        self.assertEqual(n_futsal,1)
        Form2.objects.create(Nama='abc')
        n_traveling = Form2.objects.all().count()
        self.assertEqual(n_traveling,1)
        Form3.objects.create(Nama='abc')
        n_jogging = Form3.objects.all().count()
        self.assertEqual(n_jogging,1)

    def test_form_futsal_post_valid(self):
        response_post = self.client.post('/kegiatan', {'Nama':'abc', 'btn_form_futsal': 'Tambah Peserta'})
        self.assertEqual(response_post.status_code,200)
        form_futsal = Form_Futsal(data={'Nama':"abc"})
        self.assertTrue(form_futsal.is_valid())
        self.assertEqual(form_futsal.cleaned_data['Nama'],"abc")
    
    def test_form_coding_post_valid(self):
        response_post = self.client.post('/kegiatan', {'Nama':'abc', 'btn_form_traveling': 'Tambah Peserta'})
        self.assertEqual(response_post.status_code,200)
        form_traveling = Form_Traveling(data={'Nama':"abc"})
        self.assertTrue(form_traveling.is_valid())
        self.assertEqual(form_traveling.cleaned_data['Nama'],"abc")

    def test_form_sepeda_post_valid(self):
        response_post = self.client.post('/kegiatan', {'Nama':'abc', 'btn_form_jogging': 'Tambah Peserta'})
        self.assertEqual(response_post.status_code,200)
        form_jogging = Form_Jogging(data={'Nama':"abc"})
        self.assertTrue(form_jogging.is_valid())
        self.assertEqual(form_jogging.cleaned_data['Nama'],"abc")
