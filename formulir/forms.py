from django import forms
from .models import Form1, Form2, Form3

class Form_Futsal(forms.ModelForm):
    class Meta:
        model = Form1
        fields = [
            'Nama',
        ]

class Form_Traveling(forms.ModelForm):
    class Meta:
        model = Form2
        fields = [
            'Nama',
        ]

class Form_Jogging(forms.ModelForm):
    class Meta:
        model = Form3
        fields = [
            'Nama',
        ]