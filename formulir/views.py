from django.shortcuts import render
from .forms import Form_Futsal,Form_Traveling,Form_Jogging
from .models import Form1,Form2,Form3

# Create your views here.
def kegiatan(request):
    if request.method == 'POST' and 'btn_form_futsal' in request.POST:
        form_futsal = Form_Futsal(request.POST)
        if form_futsal.is_valid():
            form_futsal.save()
    elif request.method == 'POST' and 'btn_form_traveling' in request.POST:
        form_traveling = Form_Traveling(request.POST)
        if form_traveling.is_valid():
            form_traveling.save()
    elif request.method == 'POST' and 'btn_form_jogging' in request.POST:
        form_jogging = Form_Jogging(request.POST)
        if form_jogging.is_valid():
            form_jogging.save()

    form_futsal = Form_Futsal()
    form_traveling = Form_Traveling()
    form_jogging = Form_Jogging()
    peserta_futsal = Form1.objects.all()
    peserta_traveling = Form2.objects.all()
    peserta_jogging = Form3.objects.all()
    context = {
        'list_peserta_futsal' : peserta_futsal,
        'list_peserta_traveling' : peserta_traveling,
        'list_peserta_jogging' : peserta_jogging,
        'form_futsal' : form_futsal,
        'form_traveling' : form_traveling,
        'form_jogging' : form_jogging,
        }
    return render(request,'kegiatan.html',context)